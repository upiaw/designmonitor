# DesignMonitor



## STATUS: In development

The Web App is currently in development. You can check out the status in the development branch. When the time comes, the README will have installation guides on how to deploy the app via Docker.

## License
This project is licensed under the GNU General Public License, Version 3.
